title: Learn more about Mobilizon…
clic: (click each question to reveal the answer)
prez:
  title: Presentation of Mobilizon
  what:
    q: What is Mobilizon?
    a: |-
      It’s software that, once installed on a server, will create a website where
      people can create events, similar to Facebook events or MeetUp.

      Installing Mobilizon will allow communities to free themselves from the
      services of tech giants by creating their own event platform. This installation
      (called “instance“) can easily interconnect with others like it, thanks
      to [a decentralised federation protocol](https://en.wikipedia.org/wiki/ActivityPub).

      Although it can be used to organize a simple birthday party or a karate
      competition, Mobilizon will be developed to meet the needs of activists,
      who require specific tools to organize themselves in an autonomous and sustainable
      way.
  facebook:
    q: Mobilizon will not replace Facebook
    a: |-
      By design, Mobilizon is not meant to be a “Facebook killer.”

      We can see the danger of publishing an activism rally on Facebook, a monopolistic
      platform that [multiplies scandals](https://dayssincelastfacebookscandal.com)
      about privacy and the manipulation of public opinion.
      But compared to this tech giant, our means are modest.

      Our ambition goes accordingly: let’s start with a tool that does little
      but does it well, and that will make a solid foundation for building more.
      Let’s focus first on the specific needs of a particular audience (activists),
      which won’t prevent Mobilizon from being useful in other cases. Then, in
      the long run, we can adapt it further to the needs of other audiences.

      Making Mobilizon is therefore not a sprint, where we promise everything
      at once to everyone. It’s more of a cross-country race, where the first
      step is to develop a tool that empowers anyone to create events without
      a fuss.
  features:
    q: Will Mobilizon allow me to…?
    a: |-
      Make recurring events? Share my events on Twitter? Send private messages?
      Our ideas and wishes for Mobilizon are as numerous as our supporters… There
      is but one answer:

      It depends! :)

      The list of features for V1 will depend on the amount of money raised.

      And what happens after V1 will depend on the Mobilizon community.

      Though this campaign’s success is more dependent on you than on us, we can
      promise that we will keep you informed of Mobilizon’s progress at each new
      stage, on [the news page here](@:link.joinmobilizon@:lang/news) and
      in our dedicated newsletter.
  advantages:
    q: What are the 3 benefits of using Mobilizon?
    a: |-
      **Free**: Mobilizon’s licence guarantees respect of the fundamental
      freedoms of the people who will use it. Since [its source code is
      public](@:git.mobilizon), anyone can audit it, which guarantees its transparency.
      If the direction given by the development team does not suit you, you have
      the legal right to create your own version of the software, with your own
      governance choices.

      **Ethical**: Mobilizon is developed on a non-profit basis, so there
      will be no profiling nor attention-grabbing mechanism. On the contrary,
      it is designed to give maximum power to the people who use it.

      **Human**: Mobilizon is not developed by a secretive start-up, but by
      a group of friends who strive to [change the world, one byte at a time](@:link.soft).
      So while we do work slower, we remain attentive and in touch with our users.
      Moreover, Mobilizon was designed by asking activists how they use digital tools.
  federated:
    q: Why is a federated tool better?
    a: |-
      Let’s imagine that my university creates its instance *MobilizeCollege*
      on the one hand, and my climate defence movement creates its instance *EcoMobilized*
      on the other hand: do I need to create an account on each site, just to
      keep up with the events?

      No: in our opinion, this would be a major obstacle to use. That’s why we
      want Mobilizon to be federated: each instance (each event publication website)
      powered by Mobilizon will be able to choose to exchange with other instances,
      to display more events than the ones it hosts, and to promote interaction.

      The federation protocol, which uses the most widespread standard
      ([ActivityPub](https://en.wikipedia.org/wiki/ActivityPub)), will also allow,
      in the long run, to build bridges with [Mastodon](https://joinmastodon.org)
      (the free and federated alternative to Twitter), [PeerTube](https://joinpeertube.org)
      (the free and federated alternative to YouTube), and many other alternative
      tools.
  install:
    q: How do I install Mobilizon on a server?
    a: |-
      For now, you can’t: the code is still under development and there are no
      installation guidelines yet.

      You will need to wait for the release of the beta version (fall 2019) and
      version 1 (first half of 2020) to see installation facilitated by complete
      documentation, and even packaging.

      However, if you want to keep an eye on our work in progress, [the source
      code is here](@:git.mobilizon).
  develop:
    q: How can I contribute code to Mobilizon?
    a: |-
      First of all, you will need knowledge of Git and Elixir. If you are not
      familiar with them, the project is not in a position to receive your contributions
      for the time being.

      If you do, simply go to [the software repository](@:git.mobilizon)
      and send in an issue, or fork the code to start
      submitting your own contributions.

      Of course, it’s always better to come and talk with our developers beforehand,
      by using [our Matrix](https://riot.im/app/#/room/#Mobilizon:matrix.org) room.
  contribute:
    q: How can I contribute to Mobilizon if I’m not a programmer?
    a: |-
      The easiest way is to come and talk to us, in [the Mobilizon
      area](https://framacolibri.org/c/mobilizon) of our contributions forum.

      Remember that we are not a multinational tech giant, nor even a large start-up,
      but a mere not-for profit with fewer than 40 members (which also manages
      other projects in parallel): don’t be offended if we need time to get back
      to you!

      You probably have some great ideas to add to the project, and we thank you
      for taking the time to share them. However, we know that we can deliver
      what we promised for the beta version and V1, but we also know that we cannot
      add to our already ambitious schedule.

      Thus, you should expect that your wishes and proposals will not be implemented
      until after version 1, which we (Framasoft) hope to be able to bring to
      completion (but that will depend on the success of this crowdfunding campaign!).
  cost:
    q: How much will it cost me to use Mobilizon?
    a: |-
      The aim of crowdfunding the creation of this software is that a large number
      of organisations can then make it available for free, as is done for most
      [Mastodon](https://joinmastodon.org) (the free and federated
      alternative to Twitter) and [PeerTube](https://joinpeertube.org)
      (the free and federated alternative to YouTube) instances.

      What is certain is that the Mobilizon software will be distributed free
      of charge and that no one will have to pay us anything to install it on
      their servers (and we will even try to make your lives easier!).

      Our hope is that the enthusiasm around this service gives us the means to
      open our own instance, public and free of use, perhaps when V1 is released…
  timeline-q:
    q: When can I use Mobilizon?
    a: |-
      As of today, the code is still under development, and cannot be simply and
      conveniently installed and used.

      In the fall of 2019, we plan to release a beta version, whose features
      will depend on the amount collected during crowdfunding. This version, which
      is equivalent to a first draft, will allow unstable use: the documentation
      will be in the process of being published, and there will likely still be
      improvements to be made.

      Note that if we reach the 2<sup>nd</sup>level, we will open a demonstration
      instance to the public, where everyone can click around and experiment,
      but where data will be deleted on a regular basis.

      It is only in the first half of 2020, when version 1 is released, that Mobilizon
      will be easily usable by everyone.
crowdfunding:
  title: Crowdfunding questions
  third-platform:
    q: Why not use a platform like GoFundMe or Kickstarter?
    a: |-
      Framasoft is an <i lang="fr">association</i> domiciled in France, which
      means we can’t use many platforms that require a tax domicile in North America.
      In addition, the French platforms (which [we have already
      used](https://fr.ulule.com/etherpad-framapad), on [multiple
      occasions](https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform)
      do not offer a tool that exactly complies with the crowdfunding
      model we wanted to use this time.

      So we decided to save on the 5% commission that these intermediaries earn
      on average, and to create our own fundraising website. We already had the
      banking and accounting tools to collect donations, so we just needed to
      create the web interface.

      The advantage of having a homemade tool is that your data does not pass
      through yet another intermediary. As with all the [ethical services](@:link.dio)
      we have been offering for years, [we’re not after your data](@:link.soft/cgu),
      because we sincerely value your privacy.
  trust:
    q: How do I know if I can trust you?
    a: |-
      This is a question that must be asked, because everyone has their own levels
      of trust, which depend on several factors. We can tell you a bit about us,
      but it will be up to you to make the call.

      Framasoft is a not-for-profit association (under the French 1901 Act):
      our aim is not to make money, just to earn the means to pursue our actions
      and achieve financial balance. So we have no interest in exploiting your
      data, profiling you, etc.

      Framasoft’s business model is generosity: we are funded through individuals’
      donations and have been for years. Our accounts are audited by an independent
      auditor, whose [reports are published here (in French)](@:link.soft/association)
      (the 2018 report is currently being processed).

      Our actions are known and recognised throughout the French-speaking free
      software community. From our [free resources directory](@:link.libre)
      to our [free licenced book publishing house](@:link.book), to the [34
      alternative services](@:link.dio) that we host to Degooglise the Internet,
      time and again we have demonstrated our commitment to strong ethics.

      After more than 15 years of existence, Framasoft’s reputation is well established
      (look us up!). We have already had crowdfundings to finance the development
      of [MyPads](https://fr.ulule.com/etherpad-framapad/) and [PeerTube](https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform),
      which we have delivered [beyond what was promised](@:link.joinpeertube/news)
      (and funded) during those campaigns.
  noreward:
    q: Why don’t you offer any rewards?
    a: |-
      Because the true goal for all of us is the creation of a digital *common*.
      Indeed, Mobilizon is distributed [under a free
      licence](@:git.mobilizon/blob/master/LICENSE), which means that it belongs
      to everyone, without anyone being able to appropriate it exclusively.

      The reward or perk system of traditional crowdfunding costs a lot of time,
      money and energy to project leaders. But we are betting that enough people
      will be willing to fund a credible alternative to Facebook events, just
      for the sake of making it available to everyone.

      Lastly, we don’t want a reward system that discriminates between contributors
      based on their income, because everyone has different means, and everyone
      can participate, according to their means, in the birth of a digital common.

      Thus, any person who contributes to financing Mobilizon will be entitled
      (if they so wish) to:
      - get their (nick)name added to Mobilizon’s readme file and Joinmobilizon.org’s
        Hall of Fame;
      - receive the Mobilizon newsletter.
  receipt:
    q: Will I get a receipt for my donation?
    a: |-
      Yes, provided that you tick the appropriate box on the donation form and
      provide us with a mailing address, which is required by law for us to issue
      a donation receipt.

      Your receipt will be automatically generated in March 2020, and sent to
      the email address you have provided.

      If you need a receipt before this date, or if you change your email address,
      you can [contact us here](https://contact.framasoft.org).
  taxes:
    q: Will my donation be tax deductible?
    a: |-
      Only for French taxpayers.

      Framasoft being an “association of public interest” (under French classification),
      any donation can entitle you to [a 66% reduction
      (FR)](@:link.blog/2018/11/22/impots-et-dons-a-framasoft-le-prelevement-a-la-source-en-2019/)
      on your income taxes.

      Thus, a “UX interview” donation of €130 would, after deduction, cost French
      taxpayers €44.20.

      If you do not pay income taxes in France, please check with your country’s
      tax authorities.
